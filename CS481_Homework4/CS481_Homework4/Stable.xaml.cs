﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;



namespace CS481_Homework4
{
    // [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Stable : ContentPage
    {

        private bool _isRefreshing = false;
        public bool IsRefreshing
        {
            get { return _isRefreshing; }
            set
            {
                _isRefreshing = value;
                OnPropertyChanged(nameof(IsRefreshing));
            }
        }


        public Animal[] animals =
        {
            new Animal("Magpie Goose", "A flock of magpie geese lives in Walkabout Australia’s walk-through area, near the pond. ", 
                "Magpie_Goose.jpg"),
            new Animal("Kangaroos and Wallabies", "At Walkabout Australia, you can walk among western gray kangaroos and red-necked wallabies",
                "Kangaroos.jpg"),
           new Animal("Ostrich", "Look for these large denizens of Africa during an Africa Tram tour or Caravan Safari.",
                "Ostrich.jpg"),
            new Animal("Coati", "You can witness the agile antics of these animals in their habitat beside Thorntree Terrace. ",
                "coati.jpg"),
            new Animal("Radjah Shelduck", "They are skilled fliers that have been described as more like geese than ducks—and they are equally good runners.",
                "radjahshelduk.jpg")

    };

        public Stable()
        {
            InitializeComponent();
            ObservableCollection<Animal> listOfAnimals = new ObservableCollection<Animal>(animals);
            AnimalsList.ItemsSource = listOfAnimals;
        }

        //Handle the refreshing of each page
        void Handle_Refreshing(Object sender, System.EventArgs e)
        {
            AnimalsList.IsRefreshing = false;
        }
        //opens main page
        private async void Main_Click(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new MainPage());
        }
        //opens new threatended page to display animals that are threatended
        private async void Threatended_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new Threatened());
        }
        //opens new Endangered page to display animals that are endangered
        private async void Endangered_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new Endangered());
        }

        public void OnItemTapped(object sender, ItemTappedEventArgs args)
        {
        }

    }
}