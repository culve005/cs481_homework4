﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;



namespace CS481_Homework4
{
    // [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Threatened : ContentPage
    {
        private bool _isRefreshing = false;
        public bool IsRefreshing
        {
            get { return _isRefreshing; }
            set
            {
                _isRefreshing = value;
                OnPropertyChanged(nameof(IsRefreshing));
            }
        }


        public Animal[] animals =
        {
            new Animal("Lion", "A lion’s life is all about sleeping, napping, and resting.", "lions.jpg"),
            new Animal("Southern Cassowary", "One of the largest birds on the planet. ",
                "cassowary.jpg"),
           new Animal("Secretary Bird", "Standing over four feet tall in the tall grass of Africa",
                "secretary_bird.jpg"),
           new Animal("Elephant", "You can travel to Africa, of course, but the easiest way to observe African elephants is at the Safari Park!",
                "elephants.jpg"),
            new Animal("Platypus", "With a duck-like bill, a fur coat, big webbed feet, and a paddle-shaped tail, the platypus looks like no other mammal.",
                "platypus.jpg")
    };

        public Threatened()
        {
            InitializeComponent();
            ObservableCollection<Animal> listOfAnimals = new ObservableCollection<Animal>(animals);
            AnimalsList.ItemsSource = listOfAnimals;
        }

        //Handle the refreshing of each page
        void Handle_Refreshing(Object sender, System.EventArgs e)
        {
            AnimalsList.IsRefreshing = false;
        }

        private async void Main_Click(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new MainPage());
        }
        //opens new Stable page to display animals that are stable
        private async void Stable_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new Stable());
        }
        //opens new Endangered page to display animals that are endangered
        private async void Endangered_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new Endangered());
        }

        public void OnItemTapped(object sender, ItemTappedEventArgs args)
        {
        }

    }
}