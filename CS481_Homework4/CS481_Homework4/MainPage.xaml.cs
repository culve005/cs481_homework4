﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;



namespace CS481_Homework4
{
    // [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MainPage : ContentPage
    {
        private bool _isRefreshing = false;
        public bool IsRefreshing
        {
            get { return _isRefreshing; }
            set
            {
                _isRefreshing = value;
                OnPropertyChanged(nameof(IsRefreshing));
            }
        }


        public Animal[] animals =
        {
            new Animal("SD Zoo Safari Park", "San Diego Zoo Global is a conservation organization committed to saving species around the world. " +
                "Learn how we are working toward that goal.", "Img_1.png")
         };

        public MainPage()
        {
            InitializeComponent();
            ObservableCollection<Animal> listOfAnimals = new ObservableCollection<Animal>(animals);
            AnimalsList.ItemsSource = listOfAnimals;
        }

        //Handle the refreshing of each page
        void Handle_Refreshing(Object sender, System.EventArgs e)
        {
            AnimalsList.IsRefreshing = false;
        }

        //opens new Stable page to display animals that are stable
        private async void Stable_Click(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new Stable());
        }
        //opens new Threatended page to display animals that are threatended
        private async void Threatended_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new Threatened());
        }
        //opens new Stable page to display animals that are endangered
        private async void Endangered_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new Endangered());
        }

        public void OnItemTapped(object sender, ItemTappedEventArgs args)
        {
        }

        //opens the sd safari park website to ticket page
        public void Tickets_Clicked(object sender, ItemTappedEventArgs args)
        {
            Device.OpenUri(new Uri("https://www.sdzsafaripark.org/tickets"));
        }
    }
}