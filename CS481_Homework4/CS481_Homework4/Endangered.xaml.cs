﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;



namespace CS481_Homework4
{
    // [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Endangered : ContentPage
    {
        private bool _isRefreshing = false;
        public bool IsRefreshing
        {
            get { return _isRefreshing; }
            set
            {
                _isRefreshing = value;
                OnPropertyChanged(nameof(IsRefreshing));
            }
        }


        public Animal[] animals =
        {
            new Animal("Matschie’s Tree Kangaroo", "Safari Park guests can see tree kangaroos in " +
                "their habitat in Walkabout Australia.", "matschies.jpg"),
            new Animal("California Condor", "Most of our condors live in their off-exhibit “condor-minium” at the Safari Park. ",
                "cacondor.jpg"),
           new Animal("Crowned Crane", "You don’t need to “crane” your neck to get a good look at these elegant birds.",
                "crane.jpg"),
           new Animal("Tiger", "Tiger Trail’s Sambutan Longhouse offers a resting spot for guests, along with more views of the tigers. A play area lets kids romp like tiger cubs.",
                "tigers.jpg"),
           new Animal("Black Rhinos", "Northern white rhinos are extinct in the wild, and only two adult females are left on Earth.",
                "black_rhino.jpg")

    };


        public Endangered()
        {
            InitializeComponent();
            ObservableCollection<Animal> listOfAnimals = new ObservableCollection<Animal>(animals);
            AnimalsList.ItemsSource = listOfAnimals;
        }

        //Handle the refreshing of each page
        void Handle_Refreshing(Object sender, System.EventArgs e)
        {
            AnimalsList.IsRefreshing = false;
        }

        private async void Main_Click(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new MainPage());
        }
        //opens new Stable page to display animals that are stable
        private async void Stable_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new Stable());
        }
        //opens new Threatended page to display animals that are threatended
        private async void Threatended_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new Threatened());
        }

        public void OnItemTapped(object sender, ItemTappedEventArgs args)
        {
        }


    }
}