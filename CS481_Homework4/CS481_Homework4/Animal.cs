﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CS481_Homework4
{
    public class Animal
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public string A { get; set; }
        public string Img1 { get; set; }
        public string Img2 { get; set; }
        public string Img3 { get; set; }
        public string Img4 { get; set; }


        public Animal(string title, string description, string a)
        {
            Title = title;
            Description = description;
            A = a;
        }
    }
}